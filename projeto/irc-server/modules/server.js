var net = require('net'),
    ServerCommands = require('./comandosServidor');


function Server() {
    this.port = 6667;
    this.commands = new ServerCommands(this);
    this.connectionsCount = 0;
    this.lastCommand = "";
}

Server.initialize = function () {
    var server = new Server();
    server.start();
};

Server.prototype = {
    version: '0.0.1',

    start: function () {
        var server = this;
        this.server = net.createServer(conn);
        this.server.listen(this.port);
        console.log('Servidor escutando em: ' + this.port);

        function conn(client) {
            server.connectionsCount++;
            client.on("data", function(line) {
                server.data(client, line.toString());
            });
        }
    },

    data: function (client, line) {
        this.send(line, client);
    },

    parseMessage: function (data) {
        var message = data.toUpperCase().trim().split(/ :/),
            args = message[0].split(' ');

        message = [message.shift(), message.join(' :')];

        if (message.length > 0) {
            args.push(message[1]);
        }

        if (data.match(/^:/)) {
            args[1] = args.splice(0, 1, args[1]);
            args[1] = (args[1] + '').replace(/^:/, '');
        }

        return {
            command: args[0],
            args: args.slice(1)
        };
    },

    send: function (data, client) {
        var message = this.parseMessage(data);
        if (this.findCommand(message.command, message.args)) {
            this.commands[message.command].apply(this.commands, [client].concat(message.args));
        }
    },

    findCommand: function (command, args) {
        this.lastCommand = command + " " + args.toString().replace(",", " ").trim();
        return this.commands[command];
    }
};

exports.Server = Server;

if (!module.parent) {
    Server.initialize();
}